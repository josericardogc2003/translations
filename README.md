# Translations

Translation files for www.mapban.gg

# How to help
If you want to help us translating Mapban, check the folder "translations". Also read the "CONTRIBUTING.md" file.

## Path Translations

Inside this folder you will find a file for each language (except english). The Content of this file should look like this:

Example: de.txt

```
[MULTILANG.rules.de]
contact = /kontakt
about = /ueberuns
mediapack = /medienpaket
```

These are translations for global URL paths of Mapban. This allows us to make a few path readable in the translated language.
so when translating these, make sure not to use special characters (üéä etc.) or spaces.

## Dictionary

Also inside the translations folder you will find another folder called "dict" (short for dictionary). Inside this folder you have a file for each language.
The name of the language is always the "ISO 639-1"-code as lowercase-uppercase.ini (for example de-DE.ini). You can find all "ISO 639-1"-codes [here](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)!

The only language that is excluded from this rule is english, the english file just called en.ini. This is because this file is used as fallback when a translation isn't found in the selected language.

# Questions

If you have any questions contact us in discord. https://discordapp.com/invite/QV2kthA